#include "parameters.h"

/*Define Midpoint Function*/
double midp(double xi, double h)
{
        return pow((xi + h*0.5), (double)7);
}

/*Define Trapezoid Function*/
double trap(double xi,double h)
{
        return pow(xi, (double)7) + pow((xi + h), (double) 7);
}

/*Define Simple Function*/
double simp(double xi,double h)
{
        return pow((xi-h), (double)7) + 4*pow(xi, (double)7) + pow((xi+h), (double)7);
}
                 

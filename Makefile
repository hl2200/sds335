CC := gcc
CFLAGS := -O3
LDLIBS := -lm

SRC := $(wildcard *.c)
OBJ := $(patsubst %.c, %.o, $(SRC))

EXEC := integrate
$(EXEC): $(OBJ)
	$(CC) $(LDLIBS) -o $@ $^

prog.o routines.o: parameters.h

.PHONY: clobber clean neat echo
clobber: clean
	$(RM) $(EXEC)
clean: neat
	rm *.o integrate
neat:
	$(RM) *~ .*~
echo:
	@echo $(OBJ)

#include "parameters.h"

int main()
{
	printf("%-10s %-15s %-15s %-15s %-15s %-15s %-15s\n", "N", "Midpt", "Trapz", "Simps", "midpt_err", "trap_err", "simps_err");
	int Ns[7]={11, 101, 1001, 10001, 100001, 1000001, 10000001};
	int i, j, k=1;
	double N, h, midp_i, trap_i, simp_i, midp_err, trap_err, simp_err, xi;
        int x_start, x_end;

	for (i=0; i < 7; i++)
	{
		N=Ns[i];
		h=1/N;
		midp_i=0;
		trap_i=0;
		simp_i=0;
		x_start=0;		
		x_end=N;
		xi=0;
	
		for (j=x_start; j < x_end; j++)
		{
			midp_i += midp(xi, h);
			trap_i += trap(xi, h);
			xi+=h;
		}

		midp_i *= h;
		trap_i *= h/2;
		
		h=1/(N-1);
		xi=h;
		for (k=x_start; k < x_end-1; k+=2)
		{
			simp_i += simp(xi, h);
			xi+=2*h;
		}

		simp_i *= h/3;
		midp_err=0.125-midp_i;
		trap_err=0.125-trap_i;
	        simp_err=0.125-simp_i;
		printf("%10d %13.13f %13.13f %13.13f %13.13f %13.13f %13.13f\n", Ns[i], midp_i, trap_i, simp_i, midp_err, trap_err, simp_err);
	}
	return 0;
}

